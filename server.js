const esl = require('modesl'),
    esl_server = new esl.Server({port: 8085, myevents:true}, function() {
        console.log("esl server is up at port 8085");
    });

const express = require('express'),
    app = express(),
    fs= require('fs'),
    util = require('util'),
    server = app.listen(8086, ()=> {
        console.log("express server is up at port 8086");     
        console.log(" Call ID   | Start Time    | End Time  | Duration  | From Number   | To Number | User ID   |");
        console.log("___________|_______________|___________|___________|_______________|___________|___________|")   
    });

let calls = {};

class CallProcessor {
    constructor(conn, id) {
        // console.log('new call ' + id);
        let instance = this;
        this.call_start = new Date().getTime();

        this.wsconnect = () => {
            conn.execute('set',"P-wsbridge-websocket-uri=ws:\/\/3.20.32.218:3000");
            conn.execute('set',"P-wsbridge-websocket-content-type=audio%2Fl16%3Brate%3D16000");
            conn.execute('set',`P-wsbridge-websocket-headers={ \
                "fsip":"3.20.32.218", \
                "call-id": "${id}", \
                "text":"hello there. hows it going?" \
            }`);
            conn.execute('bridge','wsbridge');
        };

        this.echo = () => {
            conn.execute('answer');
            conn.execute('echo', function(){
                console.log('echoing');
            });
        };

        this.cdr = () => {
            instance.call_end = new Date().getTime();
            instance.duration = (instance.call_end - instance.call_start) / 1000;
            console.log(`${id}\t|${instance.call_start}\t|${instance.call_end}\t|${instance.duration}`);

            // Free up memory by deleting the object from stack
            calls[id] = null;
            delete calls[id];
        };

        this.hangup = () => {
            conn.execute('hangup',(res)=> {
                // instance.cdr();
            });
        };
    };
};

app.get('/hangup/:callid',(req,res)=> {
    calls[req.params.callid].hangup();
    res.status(200).end(`Call with ID {req.params.callid} terminated`);
    // res.send('Call termination in progress!');
});

esl_server.on('connection::ready', function(conn, id) {

    calls[id] = new CallProcessor(conn,id);
    calls[id].wsconnect();
    console.log(`ModESL ID = ${id}`);

    // fs.writeFileSync("abc.txt",(util.inspect(conn['channelData']['headers'],{showHidden:true,depth:null})));
    
    conn.on('esl::end', function(evt, body) {
        calls[id].cdr();
    });

});
